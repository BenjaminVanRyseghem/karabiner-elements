#!/bin/sh

mkdir ~/.config/karabiner

for file in $( ls ); do
    ln -s $(pwd)/$file ~/.config/karabiner/$file
done
